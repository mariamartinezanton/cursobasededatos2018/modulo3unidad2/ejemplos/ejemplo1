﻿-- Listar las edades de los ciclistas ( sin apellidos) --

SELECT DISTINCT edad FROM CICLISTA;

-- listar las edades de los ciclistas de artiach --

SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach';

--listar las edades de ariach o de amore vita --

SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach' OR 'amore vita';

--listar los dorsales de los ciclistas cuya edad sea menor de 25 o mayor de 30 -- 

SELECT dorsal FROM ciclista WHERE edad <25 OR edad >30;

-- listar los dorsales de los ciclistas cuya edad este entre 28 Y 32 Y ademas que solo sean de Banesto --

SELECT dorsal FROM ciclista WHERE nomequipo='Banesto' AND edad BETWEEN 28 and 32;

-- Indícame el nombre de los ciclistas cuyo numero de caracteres sea mayor de 8 --

SELECT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8;

-- listame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado mayusculas que debe mostrar el nombre en mayusculas --

  SELECT nombre, dorsal, upper(nombre) AS _NOMBRE_ FROM ciclista;

-- listar todos los ciclistas que han llevado el mailot MGE(amarillo) en alguna etapa --

  SELECT DISTINCT dorsal FROM lleva WHERE código='MGE';

--listar el nombre de los puertos cuya altura sea mayor de 1500 --

SELECT nompuerto FROM puerto WHERE altura >1500;

-- listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000 --

SELECT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000;

-- listar el dorsal de los ciclistas que hayab ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté ente 1800 y 3000 --

  SELECT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000;

